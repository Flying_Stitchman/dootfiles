#!/bin/bash
# Mute Toggle Script


# Toggle Mute State
$(wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle)

# Query Pipewire for Mute State
mute="$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | awk '{print $3}')"

# Send Different Notification if Muted or Not
if [[ "$mute" == "" ]]; then
	# Send the notification
	$(dunstify "Unmuted" -i ${XDG_CONFIG_HOME:-~/.config}/dunst/images/volumeUnMute.png -h string:x-dunst-stack-tag:volume)
else
	# Send the other notification
	$(dunstify "Muted" -i ${XDG_CONFIG_HOME:-~/.config}/dunst/images/volumeMute.png -h string:x-dunst-stack-tag:volume)
fi
