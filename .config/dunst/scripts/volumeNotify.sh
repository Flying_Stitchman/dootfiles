#!/bin/bash
#Change Volume Script


# Query Pipewire and Parse Mute State
mute="$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | awk '{print $3}')"

# Check to see if it is muted, if so, don't change volume.
# Otherwise, change volume and Send the notification.
if [[ "$mute" == "" ]]; then
	# Change the volume
	$(wpctl set-volume @DEFAULT_AUDIO_SINK@ $@)

	# Query Pipewire and Parse Volume as an Int 
	volume="$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | awk '{print $2}' | sed 's/[^0-9]*//g')"
	
	# Send the notification
	$(dunstify -h int:value:"$volume" Volume: $volume -h string:x-dunst-stack-tag:volume)
else
	# Send the other notification
	$(dunstify -i ${XDG_CONFIG_HOME:-~/.config}/dunst/images/volumeMute.png "Volume Muted" -h string:x-dunst-stack-tag:volume)
fi
