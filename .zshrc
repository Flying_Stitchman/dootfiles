# Lines configured by zsh-newuser-install
HISTFILE=~/.local/share/histfile
HISTSIZE=1000
SAVEHIST=1000
setopt extendedglob nomatch
unsetopt autocd beep notify
unsetopt PROMPT_SP
bindkey -v
# End of lines configured by zsh-newuser-install

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

# Aliases
alias kon="ncmpcpp -b ~/.config/ncmpcpp/bindings"
alias myip="curl ifconfig.me"
alias ghidra="_JAVA_AWT_WM_NONREPARENTING=1 ghidra"
alias ls='exa' # makes ls colorful
alias v="nvim"   # Launch neovim with 'v'
alias mountUSB="mount /home/flyingstitchman/ExternalDrive" # Automagically mount and unmount my flashdrive
alias umountUSB="umount /home/flyingstitchman/ExternalDrive"
alias dotfiles='/usr/bin/git --git-dir=$HOME/Documents/Repos/dootfiles/.git --work-tree=$HOME'
alias unityAvi="/home/flyingstitchman/Documents/Unity/Editors/2019.4.31f1/Editor/Unity -projectPath ~/ExternalDrive/Unity\ Projects/Avatar\ 3.0\ 2019"
alias ssh="TERM=xterm ssh"   #Fix foot issue in ssh
alias gdb="gdb -x /usr/share/pwndbg/gdbinit.py" #pwntools for GDB
alias l="ls -C | sort -k1,1"
alias make="make -j 20"
alias cd="z"
alias imv="imv -b aa77bb"
alias find="fd"
alias tree="exa --tree"
alias sshh='ssh -i ~/.ssh/panoply'



# Automatically get new programs for autocompletion

zshcache_time="$(date +%s%N)"

autoload -Uz add-zsh-hook

rehash_precmd() {
  if [[ -a /var/cache/zsh/pacman ]]; then
    local paccache_time="$(date -r /var/cache/zsh/pacman +%s%N)"
    if (( zshcache_time < paccache_time )); then
      rehash
      zshcache_time="$paccache_time"
    fi
  fi
}

add-zsh-hook -Uz precmd rehash_precmd

# Fancy Term Colors

# Load promptinit
autoload -Uz promptinit && promptinit

# Define the theme
prompt_mytheme_setup() {
  PS1=" %F{#C250B0}Flying%f%F{#60F060}_%f%F{#50C0C0}Stitchman%f %F{#40F040}%B%~%b%f "
}

# Add the theme to promptsys
prompt_themes+=( mytheme )

# Load the theme
prompt mytheme

 # The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' completions '    '
zstyle ':completion:*' max-errors 3

autoload -Uz compinit
compinit
# End of lines added by compinstall
eval "$(zoxide init zsh)"

 
# Default Programs
export EDITOR=nvim

# Needed for pam_gnupg for ssh keys TO DO
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
export PATH="${PATH}:/home/flyingstitchman/.cargo/bin"

# Ibus
GTK_IM_MODULE=fcitx
QT_IM_MODULE=fcitx
XMODIFIERS=@im=fcitx

 # Clean Up
export QT_QPA_PLATFORM=wayland
export QT_QPA_PLATFORMTHEME=qt5ct
# Fuck Gnome Devs, All my homies Hate Gnome Devs
#export GDK_DEBUG=1
export MBSYNCRC=${XDG_CONFIG_HOME:-~/.config}/mbsync/config
export PASSWORD_STORE_DIR=${XDG_CONFIG_HOME:-~/.config}/password_store
export GNUPGHOME=${XDG_DATA_HOME-~/.config}/gnupg
export NOTMUCH_CONFIG=${XDG_CONFIG_HOME-~/.config}/notmuch/config
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
export KDEHOME="$XDG_CONFIG_HOME"/kde
export GRADLE_USER_HOME="$XDG_DATA_HOME"/gradle
export GPG_TTY=$(tty)
export SDL_VIDEODRIVER=wayland
export GOPATH="$XDG_DATA_HOME"
export GOMODCACHE="$XDG_CACHE_HOME"
export NUGET_PACKAGES="$XDG_CACHE_HOME"/NuGetPackages
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export WINEPREFIX="$XDG_DATA_HOME"/wineprefixes/default
export DOTNET_CLI_HOME="$XDG_CONFIG_HOME"/dotnet
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export CARGO_HOME="$XDG_DATA_HOME"/cargo


alias dosbox="dosbox -conf "$XDG_CONFIG_HOME"/dosbox/dosbox.conf "


# Autostart Hyprland after all environmental Variables have been declared
if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
#  export WLR_DRM_DEVICES=/dev/dri/card0
  exec Hyprland
fi

export QSYS_ROOTDIR="/home/flyingstitchman/.cache/paru/clone/quartus-free/pkg/quartus-free-quartus/opt/intelFPGA/21.1/quartus/sopc_builder/bin"
